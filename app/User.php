<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Set password hash.
     *
     * @param array User data
     * @return array
     */
    protected function setPassword($user)
    {
        if (!empty($user['password'])) {
            $user['password'] = bcrypt($user['password']);
        }

        return $user;
    }

    /**
    * Get user roles lists.
    *
    * @param array $roles
    */
    public function getUserRolesLists()
    {
        return $this->roles()->lists('roles.id')->all();
    }

    /**
     * The roles that belong to the user.
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    /**
     * Returns the model singular table name.
     *
     * @return string
     */
    public function getSingularTableName()
    {
        return str_singular($this->getTableName());
    }

    /**
     * Returns model table name.
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->getTable();
    }

    /**
     * Returns the model name.
     *
     * @return string
     */
    public function getModelName()
    {
        $reflect = new ReflectionClass($this);
        return $reflect->getShortName();
    }

    /**
     * Returns the controller name.
     *
     * @return string
     */
    public function getControllerName()
    {
        return $this->getModelName() . "Controller";
    }
}
