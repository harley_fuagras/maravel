<?php

/**
 * If a key exists return it's value, if not, return an empty string
 *
 * @param  array $array
 * @param  string $key
 * @return string
 */
function checkKey($array, $key)
{
    return array_key_exists($key, $array) ? $array[$key] : '';
}

/**
 * Formats dates in the format provided or in the default format set in maravel config file
 *
 * @param  object $date
 * @param  string $format
 * @return string
 */
function formatDate($date, $format = '')
{
    if (!$format) {
        $format = config('maravel.primary_date_format');
    }

    return $date->format($format);
}

/**
 * Gets an html move to trash button for datatables
 *
 * @param  string $page_name
 * @param  integer $id
 * @param  string $message
 * @return string
 */
function getTrashButton($page_name, $id, $message = '')
{
    $message = $message ? $message : rawUrlEncode(trans('messages.confirm_trash'));
    $data_url =  url('admin/' . $page_name . '/soft-delete/' . $id);

    return view('admin.partials.datatable-action', [
        'buttons' => [
            [
                'class' => 'fa fa-trash-o',
                'title' => trans('messages.delete'),
                'attribute' => 'data-delete data-delete-msg=' . $message . ' ' . 'data-url=' . $data_url,
            ]
        ]
    ])->render();
}

/**
 * Gets an html delete button for datatables
 *
 * @param  string $page_name
 * @param  integer $id
 * @param  string $message
 * @return string
 */
function getDeleteButton($page_name, $id, $message = '')
{

    $message = $message ? $message : rawUrlEncode(trans('messages.confirm'));
    $data_url = url('admin/' . $page_name . '/' . $id);

    return view('admin.partials.datatable-action', [
        'buttons' => [
            [
                'class' => 'fa fa-trash',
                'title' => trans('messages.delete'),
                'attribute' => 'data-delete data-delete-msg=' . $message . ' ' . 'data-url=' . $data_url,
            ]
        ]
    ])->render();
}

/**
 * Gets an html restore button for datatables
 *
 * @param  string $page_name
 * @param  integer $id
 * @return string
 */
function getRestoreButton($page_name, $id)
{
    return view('admin.partials.datatable-action', [
        'buttons' => [
            [
                'url' => url('admin/' . $page_name . '/restore/' . $id),
                'class' => 'fa fa-undo',
                'title' => trans('messages.restore'),
            ]
        ]
    ])->render();
}

/**
 * Gets an html edit button for datatables
 *
 * @param  string $page_name
 * @param  integer $id
 * @param  string $message
 * @return string
 */
function getEditButton($page_name, $id)
{
    return view('admin.partials.datatable-action', [
        'buttons' => [
            [
                'url' => url('admin/' . $page_name . '/' . $id . '/edit'),
                'class' => 'fa fa-pencil',
                'title' => trans('messages.edit'),
            ]
        ]
    ])->render();
}

/**
 * Gets an html multilanguage edit buttons for datatables
 *
 * @param  string $page_name
 * @param  integer $id
 * @param  object $model
 * @return string
 */
function getMultilanguageEditButtons($page_name, $id, $model)
{
    $buttons = [];

    foreach (config('app.locales') as $locale) {
        $is_translated = $model->hasTranslation($locale);

        $buttons[] = [
            'url' => url('admin/' . $page_name . '/' . $id . '/edit?locale=' . $locale),
            'class' => $is_translated ? '' : 'untranslated',
            'title' => trans('messages.edit'),
            'text' => $locale,
        ];
    }

    return view('admin.partials.datatable-action', ['buttons' => $buttons])->render();
}

/**
 * Gets an html with a modal edit link
 *
 * @param  string $page_name
 * @param  integer $id
 * @param  string $name
 * @return string
 */
function getEditUrl($page_name, $id, $name, $multilang = false)
{
    $lang_str = $multilang ? '/edit?locale=' . config('app.fallback_locale') : '/edit';

    $url = url('admin/' . $page_name . '/' . $id . $lang_str);

    return '<a href="' . $url . '">' . $name . '</a>';
}

/**
 * If URI matches patter return class name
 *
 * @param string|array $uri
 * @param string $class
 * @return string
 */
function setActiveNavbarLink($uri, $class = 'active')
{
    $uri = (array)$uri;

    foreach ($uri as $value) {
        if (Request::is($value)) {
            return $class;
        }
    }

    return '';
}

/**
 * Gets input value if entry exists.
 *
 * @param  object $entry
 * @param string $field
 * @param string $locale
 * @return string
 */
function getFormInput($entry, $field, $locale)
{
    $input = null;
    if ($entry) {
        $old = $entry->hasTranslation($locale) ? $entry->translate($locale)[$field] : '';
        $input = Input::old("{$locale}[$field]", $old);
    }
    return $input;
}

/**
 * Gets array with multilanguage routes.
 *
 * @return array
 */
function getMultilangRoutes()
{
    $route = Request::url();
    
    foreach (config('app.locales') as $k => $config_locale) {
        $elems[$k]['name'] = $config_locale;
        $elems[$k]['url'] = $route . '?locale=' . $config_locale;
    }
    return $elems;
}

/**
 * Return sizes readable by humans.
 *
 * @param  integer $bytes
 * @param  integer $decimals
 * @return string
 */
function human_filesize($bytes, $decimals = 2)
{
    $size = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    $factor = floor((strlen($bytes) - 1) / 3);

    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
}

/**
 * Gets datatables file row thumbnail
 *
 * @param  string $filepath
 * @param  string $mime
 * @return string
 */
function getThumbnail($filepath, $mime)
{
    if (strstr($mime, 'image/')) {
        return view('admin.partials.medias.thumbnail', [
            'filepath' => $filepath,
        ])->render();
    } else {
        if (strstr($mime, 'video/')) {
            $icon = 'fa-file-video-o';
        } elseif (strstr($mime, 'audio/')) {
            $icon = 'fa-file-audio-o';
        } elseif (strstr($mime, 'pdf') || strstr($mime, 'text')) {
            $icon = 'fa-file-text-o';
        } else {
            $icon = 'fa-file-o';
        }
        return view('admin.partials.medias.icon', [
            'icon' => $icon,
        ])->render();
    }
}
