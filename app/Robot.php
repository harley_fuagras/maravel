<?php

namespace App;

class Robot extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Returns an array with the format key(id)=>value(id) of the stored
     * robots values in db
     *
     * @return array
     */
    public static function getAllIdsInArray()
    {
        $robots = Robot::all();

        $result = [];
        foreach ($robots as $robot) {
            $result[$robot->id] = trans('messages.robots_' . $robot->id);
        }

        return $result;
    }
}
