<?php

namespace App\Http\Requests\Admin\PostType;

use App\Http\Requests\Admin\Request;

class PostTypeCreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:post_types',
        ];
    }
}
