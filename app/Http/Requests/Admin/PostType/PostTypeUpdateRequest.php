<?php

namespace App\Http\Requests\Admin\PostType;

use App\Http\Requests\Admin\Request;

class PostTypeUpdateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:post_types,name, ' . $this->id . ',id',
        ];
    }
}
