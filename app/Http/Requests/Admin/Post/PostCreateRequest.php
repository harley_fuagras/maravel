<?php

namespace App\Http\Requests\Admin\Post;

use App\Http\Requests\Admin\Request;

class PostCreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $locale = config('app.fallback_locale');

        return [
            $locale . '.title' => 'required|max:255',
            $locale . '.slug' => 'required|max:255|unique:post_translations,slug,NULL,id',
            $locale . '.seo_title' => 'max:' . config('maravel.seo_title_length'),
            $locale . '.seo_description' => 'max:' . config('maravel.seo_description_length'),
            $locale . '.robots_id' => 'required',
        ];
    }
}
