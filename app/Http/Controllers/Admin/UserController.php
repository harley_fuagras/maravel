<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\User\UserCreateRequest as CreateRequest;
use App\Http\Requests\Admin\User\UserUpdateRequest as UpdateRequest;

use App\User;
use App\Role;

use App\DataTables\UsersDataTable as DataTable;

class UserController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(new User);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DataTable $dataTable)
    {
        return parent::renderIndex($dataTable);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::getRolesList();

        return parent::renderCreate(new CreateRequest, compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $data = User::setPassword($request->all());

        $entry = $this->model->create($data);
        $entry->roles()->sync($request->roles);

        return parent::redirectStore($entry);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::getRolesList();

        $entry = $this->model->with('roles')->findOrFail($id);
        $entry['roles'] = $entry->getUserRolesLists();

        array_forget($entry, 'password');

        return parent::renderEdit(new UpdateRequest, compact('entry', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $entry = $this->model->findOrFail($id);

        $data = User::setPassword($request->all());

        if (empty($data['password'])) {
            array_forget($data, 'password');
        }

        $entry->update($data);
        $roles = $entry->roles()->sync($request->roles);

        if (!empty($roles['attached']) || !empty($roles['detached'])) {
            $entry->touch();
        }

        return parent::redirectUpdate($entry);
    }
}
