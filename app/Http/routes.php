<?php

use App\Category;
use App\MediaOriginal;
use App\Post;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an admin.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([
    'namespace' => 'Admin',
    'prefix' => 'admin'
    ], function () {
        Route::auth();
        Route::get('/', 'HomeController@index');

        // Users
        Route::resource('/user', 'UserController');

        // Roles
        Route::resource('/role', 'RoleController');

        // Post Types
        Route::resource('/post-type', 'PostTypeController');

        // Media Formats
        Route::resource('/media-extension', 'MediaExtensionController');

        // Uploads
        Route::get('/upload/media-originals', 'UploadController@getMediaOriginals');
        Route::get('/upload/media-extensions', 'UploadController@getMediaExtensions');
        Route::get('/upload/media-html', 'UploadController@getMediaHtml');

        // Posts
        $class = new Post;
        $controller = $class->getControllerName();
        $route = str_slug($class->getSingularTableName());

        Route::get('/' . $route . '/trash', $controller . '@trash');
        Route::delete('/' . $route . '/soft-delete/{id}', $controller . '@softDelete');
        Route::get('/' . $route . '/restore/{id}', $controller . '@restore');
        Route::get('/' . $route . '/order/', $controller . '@order');
        Route::post('/' . $route . '/upload', $controller . '@upload');
        Route::resource('/' . $route, $controller);

        // Media Originals
        $class = new MediaOriginal;
        $controller = $class->getControllerName();
        $route = str_slug($class->getSingularTableName());

        Route::get('/' . $route . '/trash', $controller . '@trash');
        Route::delete('/' . $route . '/soft-delete/{id}', $controller . '@softDelete');
        Route::get('/' . $route . '/restore/{id}', $controller . '@restore');
        Route::resource('/' . $route, $controller);

        // Categories
        $class = new Category;
        $controller = $class->getControllerName();
        $route = str_slug($class->getSingularTableName());

        Route::get('/' . $route . '/trash', $controller . '@trash');
        Route::delete('/' . $route . '/soft-delete/{id}', $controller . '@softDelete');
        Route::get('/' . $route . '/restore/{id}', $controller . '@restore');
        Route::get('/' . $route . '/order/', $controller . '@order');
        Route::resource('/' . $route, $controller);
    });
