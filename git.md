# Git commands

Documentation of basic git commands.

## How to work with branches

There are two main branches in our project:

* develop
* master

So, how we sould work? We have to create a new branch for each issue.

* Firstly, go to `develop` branch. We will always create new branches from `develop`

    ``` bash
    >>> git checkout develop
    ```

* Create a new branch:

    ``` bash
    >>> git checkout -b <new-branch-name>
    ```

    Branch naming convention:

    * `f/<new-branch-name>-<issue>` <-- featured issue
    * `b/<new-branch-name>-<issue>` <-- bug issue

* Apply your source changes
* Add your changes:

    ``` bash
    >>> git add *
    ```

* Commit your changes:

    ``` bash
    >>> git commit -m "<your-commit-message>"
    ```

    Commit naming convention:

    * First letter uppercase
    * Add ` #<issue-number>` at the end of the commit

* Push your changes

    ``` bash
    >>> git push origin <new-branch-name>
    ```


## Other useful git commands

``` bash
>>> git stash                           <-- stack source
>>> git stash pop                       <-- unstack source

>>> git reset HEAD^                     <-- delete last commit from remote
>>> git push origin <branch> --force    <--

>>> git commit --amend                  <-- change commit message
>>> git push origin <branch> --force    <-- apply change commit message to remote
```




