<?php

use App\User;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    /**
     * Test User login.
     *
     * @return void
     */
    public function testUserLogin()
    {
        $this->visit('/admin')
             ->type('foo@foo.com', 'email')
             ->type('foo', 'password')
             ->check('remember')
             ->press(trans('messages.login'))
             ->seePageIs('/admin/login');

        $this->visit('/admin')
             ->type('admin@admin.com', 'email')
             ->type('pancracio', 'password')
             ->check('remember')
             ->press(trans('messages.login'))
             ->seePageIs('/admin');
    }

    /**
     * Test User reset password.
     *
     * @return void
     */
    public function testUserResetPassword()
    {
        $this->visit('/admin')
             ->click(trans('messages.forgot_password'))
             ->seePageIs('/admin/password/reset');
    }

    /**
     * Test User index view.
     *
     * @return void
     */
    public function testUserIndex()
    {
        $this->visit('/admin/user')
             ->click(trans('messages.users'))
             ->seePageIs('/admin/user')
             ->click(trans('messages.add'))
             ->seePageIs('/admin/user/create');
    }

    /**
     * Test User create.
     *
     * @return void
     */
    public function testUserCreate()
    {
        $this->visit('/admin/user/create')
             ->type('Foo', 'name')
             ->type('foo@foo.com', 'email')
             ->type('123456', 'password')
             ->type('123456', 'password_confirmation')
             ->type(config('maravel.admin_role_id'), 'roles')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/user/2/edit');

        $this->visit('/admin/user/create')
             ->type('Foo', 'name')
             ->type('foo@foo.com', 'email')
             ->type('123456', 'password')
             ->type('123456', 'password_confirmation')
             ->type(config('maravel.admin_role_id'), 'roles')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/user/create');
    }

    /**
     * Test User edit.
     *
     * @return void
     */
    public function testUserEdit()
    {
        $id = $this->getLastId();

        $this->visit('/admin/user/' . $id . '/edit')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/user/' . $id . '/edit');

        $response = $this->call('PUT', '/admin/user/' . $id, [
            'locale' => 'es',
            'name' => 'Foo',
            'email' => 'foo@foo.com',
            'roles' => [
                config('maravel.admin_role_id'),
                config('maravel.user_role_id')
            ],
        ]);

        $user = User::find($id);
        $this->assertEquals($user->name, 'Foo');
        $this->assertEquals($user->email, 'foo@foo.com');

        foreach ($user->roles as $key => $role) {
            $this->assertEquals($role->id, ($key + 1));
        }
    }

    /**
     * Gets last id of users table.
     *
     * @return integer
     */
    private function getLastId()
    {
        $user = User::withTrashed()
            ->orderBy('id', 'DESC')
            ->first();

        if ($user) {
            return $user->id;
        }

        return 0;
    }
}
