<?php

use App\Post;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostTest extends TestCase
{
    /**
     * Test Post index.
     *
     * @return void
     */
    public function testPostIndex()
    {
        $this->visit('/admin/post')
             ->click(trans('messages.posts'))
             ->seePageIs('/admin/post')
             ->click(trans('messages.add'))
             ->seePageIs('/admin/post/create');

        $this->visit('/admin/post')
             ->click(trans('messages.go_to_trash'))
             ->seePageIs('/admin/post/trash');
    }

    /**
     * Test Post create.
     *
     * @return void
     */
    public function testPostCreate()
    {
        $id = $this->getLastId() + 1;
        $locale = config('app.fallback_locale');
        $robots_id = 1;

        $this->visit('/admin/post/create')
             ->type('Foo', $locale . '[title]')
             ->type('Foo', $locale . '[slug]')
             ->type('Foo', $locale . '[body]')
             ->type('Foo', $locale . '[seo_title]')
             ->type('Foo', $locale . '[seo_description]')
             ->type($robots_id, $locale . '[robots_id]')
             ->type(true, 'published')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/post/' . $id . '/edit?locale=' . $locale);

        $this->visit('/admin/post/create')
             ->type('Foo', $locale . '[title]')
             ->type('Foo', $locale . '[slug]')
             ->type('Foo', $locale . '[body]')
             ->type('Foo', $locale . '[seo_title]')
             ->type('Foo', $locale . '[seo_description]')
             ->type($robots_id, $locale . '[robots_id]')
             ->type(true, 'published')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/post/create');
    }

    /**
     * Test Post edit.
     *
     * @return void
     */
    public function testPostEdit()
    {
        $this->testPostCreate();

        $id = $this->getLastId();
        $locale = config('app.fallback_locale');
        $robots_id = 1;

        $this->visit('/admin/post/' . $id . '/edit?locale=' . $locale)
             ->press(trans('messages.save'))
             ->dontSee(trans('message.delete'))
             ->seePageIs('/admin/post/' . $id . '/edit?locale=' . $locale);

        $response = $this->call('PUT', '/admin/post/' . $id, [
            'locale' => $locale,
            'published' => true,
            $locale => [
                'title' => 'Foo1',
                'body' => 'Foo1',
                'slug' => 'Foo1',
                'seo_title' => 'Foo1',
                'seo_description' => 'Foo1',
                'robots_id' => $robots_id,
            ]
        ]);

        $post = Post::find($id);
        $this->assertEquals($post->title, 'Foo1');
    }

    /**
     * Test Post soft delete.
     *
     * @return void
     */
    public function testPostSoftDelete()
    {
        $this->testPostCreate();

        $id = $this->getLastId();

        $response = $this->call('DELETE', '/admin/post/soft-delete/' . $id);

        $this->assertEquals(302, $response->status());
        $this->notSeeInDatabase('posts', [
            'deleted_at' => null,
            'id' => $id
        ]);
    }

    /**
     * Test Post destroy.
     *
     * @return void
     */
    public function testPostDestroy()
    {
        $this->testPostCreate();

        $id = $this->getLastId();

        $response = $this->call('DELETE', '/admin/post/' . $id);
        $this->assertEquals(404, $response->status());

        $response = $this->call('DELETE', '/admin/post/soft-delete/' . $id);

        $this->assertEquals(302, $response->status());
        $this->notSeeInDatabase('posts', [
            'deleted_at' => null,
            'id' => $id
        ]);

        $response = $this->call('DELETE', '/admin/post/' . $id);

        $this->assertEquals(302, $response->status());
        $this->notSeeInDatabase('posts', [
            'id' => $id
        ]);
    }

    /**
     * Test Post trash.
     *
     * @return void
     */
    public function testPostTrash()
    {
        $this->testpostsoftDelete();

        $id = $this->getLastId();

        $this->visit('/admin/post/trash')
             ->click(trans('messages.back'))
             ->seePageIs('/admin/post');

        $response = $this->call('GET', '/admin/post/restore/' . $id);

        $this->assertEquals(302, $response->status());
        $this->seeInDatabase('posts', [
            'deleted_at' => null,
            'id' => $id
        ]);
    }

    /**
     * Gets last id of posts table.
     *
     * @return integer
     */
    private function getLastId()
    {
        $post = Post::withTrashed()
            ->orderBy('id', 'DESC')
            ->first();

        if ($post) {
            return $post->id;
        }

        return 0;
    }
}
