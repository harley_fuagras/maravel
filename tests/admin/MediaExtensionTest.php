<?php

use App\MediaExtension;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MediaExtensionTest extends TestCase
{
    /**
     * Test MediaExtension index.
     *
     * @return void
     */
    public function testMediaExtensionIndex()
    {
        $this->visit('/admin/media-extension')
             ->click(trans('messages.media_extensions'))
             ->seePageIs('/admin/media-extension')
             ->click(trans('messages.add'))
             ->seePageIs('/admin/media-extension/create');
    }

    /**
     * Test MediaExtension create.
     *
     * @return void
     */
    public function testMediaExtensionCreate()
    {
        $id = $this->getLastId() + 1;

        $this->visit('/admin/media-extension/create')
             ->type('Foo', 'name')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/media-extension/' . $id . '/edit');

        $this->visit('/admin/media-extension/create')
             ->type('Foo', 'name')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/media-extension/create');
    }

    /**
     * Test MediaExtension edit.
     *
     * @return void
     */
    public function testMediaExtensionEdit()
    {
        $id = $this->getLastId();

        $this->testMediaExtensionCreate();

        $this->visit('/admin/media-extension/' . $id . '/edit')
             ->press(trans('messages.save'))
             ->dontSee(trans('message.delete'))
             ->seePageIs('/admin/media-extension/' . $id . '/edit');

        $response = $this->call('PUT', '/admin/media-extension/' . $id, [
            'locale' => 'es',
            'name' => 'Foo'
        ]);

        $entry = MediaExtension::find($id);
        $this->assertNotEquals($entry->name, 'Foo');

        $response = $this->call('PUT', '/admin/media-extension/' . $id, [
            'name' => 'Foo1'
        ]);

        $entry = MediaExtension::find($id);
        $this->assertEquals($entry->name, 'Foo1');
    }

     /**
     * Test MediaExtension destroy.
     *
     * @return void
     */
    public function testMediaExtensionDestroy()
    {
        $this->testMediaExtensionCreate();
        $id = $this->getLastId();

        $response = $this->call('DELETE', '/admin/media-extension/' . $id);
        $this->assertEquals(302, $response->status());
        $this->notSeeInDatabase('media_extensions', [
            'id' => $id
        ]);
    }

    /**
     * Gets last id of media_extensions table.
     *
     * @return integer
     */
    private function getLastId()
    {
        $entry = MediaExtension::orderBy('id', 'desc')->first();

        if ($entry) {
            return $entry->id;
        }

        return 0;
    }
}
