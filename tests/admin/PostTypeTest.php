<?php

use App\PostType;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PostTypeTest extends TestCase
{
    /**
     * Test PostType index.
     *
     * @return void
     */
    public function testPostTypeIndex()
    {
        $this->visit('/admin/post-type')
             ->click(trans('messages.post_types'))
             ->seePageIs('/admin/post-type')
             ->click(trans('messages.add'))
             ->seePageIs('/admin/post-type/create');
    }

    /**
     * Test PostType create.
     *
     * @return void
     */
    public function testPostTypeCreate()
    {
        $id = $this->getLastId() + 1;

        $this->visit('/admin/post-type/create')
             ->type('Foo', 'name')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/post-type/' . $id . '/edit');

        $this->visit('/admin/post-type/create')
             ->type('Foo', 'name')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/post-type/create');
    }

    /**
     * Test PostType edit.
     *
     * @return void
     */
    public function testPostTypeEdit()
    {
        $id = $this->getLastId();

        $this->testPostTypeCreate();

        $this->visit('/admin/post-type/' . $id . '/edit')
             ->press(trans('messages.save'))
             ->dontSee(trans('message.delete'))
             ->seePageIs('/admin/post-type/' . $id . '/edit');

        $response = $this->call('PUT', '/admin/post-type/' . $id, [
            'locale' => 'es',
            'name' => 'Foo'
        ]);

        $post_type = PostType::find($id);
        $this->assertNotEquals($post_type->name, 'Foo');

        $response = $this->call('PUT', '/admin/post-type/' . $id, [
            'name' => 'Foo1'
        ]);

        $post_type = PostType::find($id);
        $this->assertEquals($post_type->name, 'Foo1');
    }

     /**
     * Test PostType destroy.
     *
     * @return void
     */
    public function testPostTypeDestroy()
    {
        $id = config('maravel.post_type_page_id');

        $response = $this->call('DELETE', '/admin/post-type/' . $id);
        $this->assertEquals(403, $response->status());

        $this->testPostTypeCreate();
        $id = $this->getLastId();

        $response = $this->call('DELETE', '/admin/post-type/' . $id);
        $this->assertEquals(302, $response->status());
        $this->notSeeInDatabase('post_types', [
             'id' => $id
        ]);
    }

    /**
     * Gets last id of post_types table.
     *
     * @return integer
     */
    private function getLastId()
    {
        $post_type = PostType::orderBy('id', 'desc')->first();

        if ($post_type) {
            return $post_type->id;
        }

        return 0;
    }
}
