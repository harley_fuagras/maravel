<?php

use App\Role;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoleTest extends TestCase
{
    /**
     * Test Role index.
     *
     * @return void
     */
    public function testRoleIndex()
    {
        $this->visit('/admin/role')
             ->click(trans('messages.roles'))
             ->seePageIs('/admin/role')
             ->click(trans('messages.add'))
             ->seePageIs('/admin/role/create');
    }

    /**
     * Test Role create.
     *
     * @return void
     */
    public function testRoleCreate()
    {
        $id = $this->getLastId() + 1;
        $locale = config('app.fallback_locale');

        $this->visit('/admin/role/create')
             ->type('Foo', $locale . '[name]')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/role/' . $id . '/edit?locale=' . $locale);

        $this->visit('/admin/role/create')
             ->type('Foo', $locale . '[name]')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/role/create');
    }

    /**
     * Test Role edit.
     *
     * @return void
     */
    public function testRoleEdit()
    {
        $id = config('maravel.admin_role_id');

        $locale = config('app.fallback_locale');

        $this->visit('/admin/role/' . $id . '/edit?locale=' . $locale)
             ->press(trans('messages.save'))
             ->dontSee(trans('message.delete'))
             ->seePageIs('/admin/role/' . $id . '/edit?locale=' . $locale);

        $response = $this->call('PUT', '/admin/role/' . $id, [
            'locale' => $locale,
            $locale => [
                'name' => 'Foo'
            ]
        ]);

        $role = Role::find($id);
        $this->assertEquals($role->name, 'Foo');

        $response = $this->call('PUT', '/admin/role/' . $id, [
            'locale' => $locale,
            $locale . '.name' => 'User'
        ]);

        $role = Role::find($id);
        $this->assertEquals($role->name, 'Foo');
    }

    /**
     * Test Role destroy.
     *
     * @return void
     */
    public function testRoleDestroy()
    {
        $id = config('maravel.admin_role_id');

        $response = $this->call('DELETE', '/admin/role/' . $id);
        $this->assertEquals(403, $response->status());

        $id = config('maravel.user_role_id');

        $response = $this->call('DELETE', '/admin/role/' . $id);

        $this->assertEquals(302, $response->status());
        $this->notSeeInDatabase('roles', [
            'id' => $id
        ]);
    }

    /**
     * Gets last id of roles table.
     *
     * @return integer
     */
    private function getLastId()
    {
        $role = Role::orderBy('id', 'desc')->first();

        if ($role) {
            return $role->id;
        }

        return 0;
    }
}
