<?php

use App\MediaOriginal;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MediaOriginalTest extends TestCase
{
    /**
     * Test MediaOriginal index.
     *
     * @return void
     */
    public function testMediaOriginalIndex()
    {
        $this->visit('/admin/media-original')
             ->click(trans('messages.media_originals'))
             ->seePageIs('/admin/media-original')
             ->click(trans('messages.add'))
             ->seePageIs('/admin/media-original/create');
    }

    /**
     * Test MediaOriginal create.
     *
     * @return void
     */
    public function testMediaOriginalCreate()
    {
        $this->visit('/admin/media-original/create')
             ->press(trans('messages.save'))
             ->seePageIs('/admin/media-original/create');
    }
}
