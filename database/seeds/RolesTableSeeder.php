<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'es' => [
                'name' => 'Admin',
            ],
            'en' => [
                'name' => '[EN] Admin',
            ],
        ]);

        Role::create([
            'es' => [
                'name' => 'User',
            ],
            'en' => [
                'name' => '[EN] User',
            ],
        ]);
    }
}
