<?php

use App\Robot;
use Illuminate\Database\Seeder;

class RobotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Robot::create([
            'id' => 1,
            'name' => 'index,follow',
        ]);
        Robot::create([
            'id' => 2,
            'name' => 'index,nofollow',
        ]);
        Robot::create([
            'id' => 3,
            'name' => 'nofollow',
        ]);
        Robot::create([
            'id' => 4,
            'name' => 'noindex,nofollow',
        ]);
        Robot::create([
            'id' => 5,
            'name' => 'archive',
        ]);
        Robot::create([
            'id' => 6,
            'name' => 'noindex,nofollow,noarchive,noodp,nosnippet',
        ]);
    }
}
