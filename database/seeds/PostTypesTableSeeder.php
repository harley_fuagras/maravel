<?php

use App\PostType;
use Illuminate\Database\Seeder;

class PostTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostType::create([
            'id' => config('maravel.post_type_page_id'),
            'name' => 'Página',
        ]);
        
        PostType::create([
            'id' => config('maravel.post_type_post_id'),
            'name' => 'Entrada',
        ]);
    }
}
