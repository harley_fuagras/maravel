<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe tener al menos 6 caracteres y coincidir con la confirmación.',
    'reset' => '¡Tu contraseña ha sido restablecida!',
    'sent' => '¡Recordatorio de contraseña enviado!',
    'token' => 'Este token de restablecimiento de contraseña no es válido.',
    'user' => 'No se ha encontrado ningún usuario con esa dirección de correo.',
    'email_subject' => 'Tu enlace de restablecimiento de contraseña',
    'email_text' => 'Haz clic aquí para restablecer la contraseña'

];
