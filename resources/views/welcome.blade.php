<!DOCTYPE html>
<html>
    <head>
        <title>Maravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }

            a {
                display: inline-block;
                text-decoration: none;
                margin-top: 40px;
                border: 1px solid #d0d0d0;
                padding: 10px;
                border-radius: 2px;
                font-size: 14px;
            }

            a:active {
                color: inherit;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Maravel</div>
                <a href="{{ url('/admin') }}">
                    {{ trans('messages.go_to_admin')}}
                </a>
            </div>
        </div>
    </body>
</html>
