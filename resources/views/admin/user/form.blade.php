<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('messages.content') }}</div>
            <div class="panel-body">
                @include('admin.partials.form-inputs.base', [
                    'type' => 'text',
                    'name' => 'name',
                ])
                @include('admin.partials.form-inputs.base', [
                    'type' => 'text',
                    'name' => 'email',
                ])
                @include('admin.partials.form-inputs.base', [
                    'type' => 'password',
                    'name' => 'password',
                ])
                @include('admin.partials.form-inputs.base', [
                    'type' => 'password',
                    'name' => 'password_confirmation',
                ])
                @include('admin.partials.form-inputs.base', [
                    'type' => 'select',
                    'name' => 'roles',
                    'multiple' => true,
                    'elems' => $roles,
                ])
            </div>
        </div>
    </div>
</div>
