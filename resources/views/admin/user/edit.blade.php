@extends('admin.partials.create-edit')

@section('create-edit-header')
    @include('admin.partials.crud-header', [
        'title' => [[
            'name' => trans('messages.' . $table_name),
            'url' => 'admin/' . str_slug($singular_table_name),
        ],[
            'name' => trans('messages.edit'),
        ]],
        'buttons' => [
            'add' => [
                'name' => trans('messages.add'),
                'url' => 'admin/' . str_slug($singular_table_name) . '/create',
                'class' => 'btn-success',
                'icon' => 'fa-plus-circle',
            ],
        ]
    ])
@stop

@section('create-edit-form')
    {!! Form::model($entry, [
        'url' => url('/admin/' . str_slug($singular_table_name) . '/' . $entry->id),
        'method' => 'put',
        'id' => 'create-edit-form',
        'role' => 'form',
    ]) !!}
        {!! Form::hidden('id', $value = $entry->id) !!}
        @include('admin.' . $singular_table_name . '.form', [
            'password' => trans('messages.new_password') . ' ' .trans('messages.new_password_text')
        ])
@stop

@push('scripts')
    {!! $validator !!}
@endpush
