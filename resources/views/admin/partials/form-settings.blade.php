<div class="panel panel-default">
    <div class="panel-heading">{{ trans('messages.settings') }}</div>
    <div class="panel-body">
        @if (isset($entry) && $locale_selector)
            @include('admin.partials.form-inputs.base', [
                'type' => 'select-links',
                'name' => $locale,
                'label' => trans('messages.locale'),
                'elems' => getMultilangRoutes(),
            ])
        @endif
        @if ($published)
            @include('admin.partials.form-inputs.base', [
                'type' => 'switch',
                'name' => 'published',
                'old_input' => isset($entry) ? null : true,
            ])
        @endif
    </div>
</div>
