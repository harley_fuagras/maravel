@if (!Auth::guest())
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('admin') }}">
                    {{ config('maravel.title') }}
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url('admin/post-type') }}" class="{{ setActiveNavbarLink('admin/post-type*') }}">{{ trans('messages.post_types') }}</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/post') }}" class="{{ setActiveNavbarLink(['admin/post', 'admin/post/*']) }}">{{ trans('messages.posts') }}</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/category') }}" class="{{ setActiveNavbarLink(['admin/category*']) }}">{{ trans('messages.categories') }}</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/user') }}" class="{{ setActiveNavbarLink('admin/user*') }}">{{ trans('messages.users') }}</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/role') }}" class="{{ setActiveNavbarLink('admin/role*') }}">{{ trans('messages.roles') }}</a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle {{ setActiveNavbarLink('admin/media*') }}" type="button" id="dropdownMedias" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            {{ trans('messages.medias') }}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMedias">
                            <li>
                                <a href="{{ url('admin/media-extension') }}">{{ trans('messages.media_extensions') }}</a>
                            </li>
                            <li>
                                <a href="{{ url('admin/media-original') }}">{{ trans('messages.media_originals') }}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('admin/logout') }}"><i class="fa fa-btn fa-sign-out"></i>{{ trans('messages.logout') }}</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@endif
