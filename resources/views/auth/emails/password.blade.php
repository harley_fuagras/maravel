{{ trans('passwords.email_text') }}: <a href="{{ $link = url('admin/password/reset', $token) . '?email=' . urlencode($user->getEmailForPasswordReset()) }}">{{ $link }}</a>
