# Maravel CMS

Maravel is a project for creating CMS applications with [Laravel](https://laravel.com/) 5.x.

## What is included?
* Default layout (Bootstrap + Gulp)
* Users
  * CRUD
  * Tests
  * login / logout
* Roles
  * CRUD
  * Tests
* Posts
  * CRUD
  * Tests
  * SEO

## Installation

1. Clone the repository:

    ``` bash
    >>> git clone git@gitlab.com:maneko/maravel.git
    ```

2. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update it

    ```bash
    >>> composer self-update
    ```

3. Run Composer:

    ```bash
    >>> php composer.phar install  <-- locally

    >>> composer install           <-- globally
    ```

4. Update Composer:

    ```bash
    >>> php composer.phar update  <-- locally

    >>> composer update           <-- globally
    ```

5. Install Node dependencies:

    ```bash
    >>> npm install
    ```

6. Run Gulp for the first time to ensure you have the latest resources

    ```bash
    >>> gulp
    ```

## Configuration

1. Create `.env` file (make a copy of `.env.example` file)

2. Create your database and configure it's access in `.env` file
3. Generate your application key:

    ```bash
    >>> php artisan key:generate
    ```

4. Run migrations and Seeds:

    ```bash
    >>> php artisan migrate --seed
    ```

5. Configure `public` and `storage` folder permissions as 755 (or 777)

    ```bash
    >>> sudo chgrp -R www-data storage bootstrap/cache
    >>> sudo chmod -R ug+rwx storage bootstrap/cache
    ```

## Default Admin User

There is a default admin user:
* email: admin@admin.com
* password: pancracio

## How to create new Users (without CRUD)

#### Via CLI:

``` bash
>>> php artisan tinker
>>> $user = new App\User;
>>> $user->name = 'Admin';
>>> $user->email = 'admin@admin.com';
>>> $user->password = Hash::make('pancracio');
>>> $user->save();
>>> exit
```

## Run PHP_CodeSniffer (Code style)

PHP_CodeSniffer detects violations of a defined set of coding standards.

#### Via CLI:

``` bash
>>> vendor/bin/phpcs --standard=PSR2 app config
```

### Code Style Fixer

You may use it to fix your code style before committing.

#### Via CLI:

``` bash
>>> vendor/bin/phpcbf --standard=PSR2 app config
```

## Run tests

Firstly, copy `database/database.sqlite.example` to `database/database.sqlite`. Then, run your tests:

#### Via CLI:

``` bash
>>> vendor/bin/phpunit
```

## TODO
View [gitlab issues](https://gitlab.com/maneko/maravel/issues).

## Git commands
View [git commands](https://gitlab.com/maneko/maravel/blob/master/git.md).

