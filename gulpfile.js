
// Load plugins
var elixir = require('laravel-elixir'),
    laravelElixirImagemin = require('laravel-elixir-imagemin'),
    del = require('del');

// Path variables
var publicPath = './public/',
    nodeModulesPath = './node_modules/',
    resourcesAssetsPath = './resources/assets/';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// 0ptimize images setup
elixir.config.images = {
    folder: 'img',
    outputFolder: 'img'
};

// Delete (unnecessary files) setup
elixir.extend('delete', function(path) {
    new elixir.Task('delete', function () {
        del(path);
    });
});

elixir(function(mix) {

    // Delete (unnecessary files)
    mix.delete([
        publicPath + 'css',
        publicPath + 'js',
        publicPath + 'img',
        publicPath + 'build',
        publicPath + 'fonts/font-awesome',
        publicPath + 'vendor/ckeditor',
        publicPath + 'vendor/bootstrap-chosen',
    ]);

    // 0ptimize images
    mix.imagemin();

    /*
    |--------------------------------------------------------------------------
    | Front-end Asset Management
    |--------------------------------------------------------------------------
    |
    */

    // Public logic goes here

    /*
    |--------------------------------------------------------------------------
    | Back-end Asset Management
    |--------------------------------------------------------------------------
    |
    */

    // Font Awesome
    mix.copy(
        nodeModulesPath + 'font-awesome/fonts',
        publicPath + 'fonts/font-awesome'
    );

    // Datatables
    mix.copy(
        resourcesAssetsPath + 'js/admin/datatables',
        publicPath + 'js/datatables'
    );

    // Ckeditor
    mix.copy(
        nodeModulesPath + 'ckeditor',
        publicPath + 'vendor/ckeditor'
    );

    // Bootstrap chosen
    mix.copy(
        nodeModulesPath + 'bootstrap-chosen',
        publicPath + 'vendor/bootstrap-chosen'
    );

    // Sass
    mix.sass('admin/admin.scss');

    // Mix scripts
    mix.scripts([
        nodeModulesPath + 'jquery/dist/jquery.min.js',
        nodeModulesPath + 'bootstrap-sass/assets/javascripts/bootstrap.min.js',
        nodeModulesPath + 'datatables/media/js/jquery.dataTables.min.js',
        nodeModulesPath + 'datatables-bootstrap3-plugin/media/js/datatables-bootstrap3.min.js',
        nodeModulesPath + 'datatables.net-responsive/js/dataTables.responsive.js',
        nodeModulesPath + 'datatables.net-buttons/js/dataTables.buttons.js',
        nodeModulesPath + 'datatables.net-buttons-bs/js/buttons.bootstrap.js',
        nodeModulesPath + 'datatables-rowreorder/js/dataTables.rowReorder.js',
        nodeModulesPath + 'bootstrap-chosen/dist/chosen.jquery-1.4.2/chosen.jquery.min.js',
        nodeModulesPath + 'dropzone/dist/min/dropzone.min.js',
        publicPath + 'vendor/jQuery-Slugify-Plugin/jquery.slugify.js',
        publicPath + 'vendor/jsvalidation/js/jsvalidation.min.js',
        publicPath + 'vendor/datatables/buttons.server-side.js',
        resourcesAssetsPath + 'js/admin/datatables.js',
        resourcesAssetsPath + 'js/admin/uploader.js',
        resourcesAssetsPath + 'js/admin/admin.js'
    ],
        publicPath + 'js/admin.js',
        './'
    );

    /*
    |--------------------------------------------------------------------------
    | Version (avoid cache issues)
    |--------------------------------------------------------------------------
    |
    */

    // Version
    mix.version([
        'css/admin.css',
        'js/admin.js',
    ]);
});
